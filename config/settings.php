<?php

declare(strict_types=1);

use \Psr\Container\ContainerInterface;
use Monolog\Logger;

return function (ContainerInterface $container) {
	$container->set('settings', function() {

		return [
			'error' => [
				// Should be set to false in production
				'displayErrorDetails' => true,

				// Parameter is passed to the default ErrorHandler
				// View in rendered output by enabling the "displayErrorDetails" setting.
				// For the console and unit tests we also disable it
				'logErrors' => true,

				// Display error details in error log
				'logErrorDetails' => true
			],
			'root' => dirname(__DIR__),
			'tmp' => dirname(__DIR__) . '/files/tmp',
			'public' => dirname(__DIR__) . '/public',
			'imageDirectory' => dirname(__DIR__) . '/public/images',
			'tmpDirectory' => dirname(__DIR__) . '/public/files',
			'imageFileTypes' => [
				'jpg',
				'jpeg',
				'png',
				'gif',
				'svg'
			],
			'views' => [
				'paths' => [
					dirname(__DIR__) . '/resources/views'
				],
				'options' => [
					// Should be set to true in production
					'cache_enabled' => false,
					'cache_path' => dirname(__DIR__) . '/files/tmp/twig',
				]
			]
		];
	});
};
?>
