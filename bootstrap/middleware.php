<?php

declare(strict_types=1);

use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Slim\Views\TwigMiddleware;


return function (App $app) {

	$app->add(Test\Middleware\flashMiddleware::class);

	// Add the Slim built-in routing middleware
	$app->addRoutingMiddleware();

	$app->add(TwigMiddleware::createFromContainer($app));

	$container = $app->getContainer();

	if (!is_null($container)) {
		$settings = $container->get('settings');

		// Catch exceptions and errors
		$app->addErrorMiddleware(
			$settings['error']['displayErrorDetails'],
			$settings['error']['logErrors'],
			$settings['error']['logErrorDetails']
		);
	}

};

