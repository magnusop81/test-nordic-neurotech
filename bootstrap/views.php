<?php

declare(strict_types=1);

use \Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;


return function (ContainerInterface $container) {
	$container->set('view', function() use ($container) {
		$settings = $container->get('settings')['views'];

		$options = $settings['options'];
		$options['cache'] = $options['cache_enabled'] ? $options['cache_path'] : false;

		return Twig::create($settings['paths'], $options);
	});
};

?>
