<?php

declare(strict_types=1);

use DI\Container;
use Slim\Factory\AppFactory;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$container = new Container();
AppFactory::setContainer($container);

$settings = require __DIR__ . '/../config/settings.php';
$settings($container);

$views = require __DIR__ . '/views.php';
$views($container);

$flash = require __DIR__ . '/flash.php';
$flash($container);

$app = AppFactory::create();

$middleware = require __DIR__ . '/middleware.php';
$middleware($app);

$routes = require __DIR__ . '/routes.php';
$routes($app);


return $app;

?>
