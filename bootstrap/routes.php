<?php

declare(strict_types=1);

use Slim\App;

return function (App $app) : void {

	$app->get(
		'/',
		'\Test\Controllers\ImageController:listImages'
	)->setName('images.list');

	$app->post(
		'/Images/Download',
		'\Test\Controllers\ImageController:download'
	)->setName('images.download');

	$app->post(
		'/Images/Upload',
		'\Test\Controllers\ImageController:upload'
	)->setName('images.upload');

};
