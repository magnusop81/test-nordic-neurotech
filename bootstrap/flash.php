<?php

declare(strict_types=1);

use DI\Container;


return function (Container $container) {
	$container->set('flash', function () {
		return new \Slim\Flash\Messages();
	});
}
?>
