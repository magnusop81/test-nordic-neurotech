# Test project for Nordic Neurotech AS

> Denne testen går ut på å bygge et system for opplastning og nedlastning
av bilder, med versjonskontrollering.


> Jeg ønsker å kunne laste opp bilder, se disse i et galleri og laste ned alle sammen. Når man laster ned skal bildene havne i en ZIP-fil, sammen med en fil som inneholder MD5 checksum av alle bildene. Man kan så bruke denne ved neste nedlastning der kun bilder som har annen md5 checksum en angitt i filen bli lastet ned.

# Setup/install

## Clone git repository
     git clone https://magnusop81@bitbucket.org/magnusop81/test-nordic-neurotech.git

## Setup web server

I believe Nginx should work fine. I personally used Apache, below is the .conf file I used.

    <VirtualHost *:80>
            ServerName test

            ServerAdmin webmaster@localhost
            DocumentRoot /var/www/test-nordic-neurotech/public

            ErrorLog ${APACHE_LOG_DIR}/error_test.log
            CustomLog ${APACHE_LOG_DIR}/access_test.log combined


            <Directory "/var/www/test-nordic-neurotech/public/">
                    AllowOverride None

                    RewriteEngine On
                    RewriteCond %{REQUEST_FILENAME} !-d
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteRule ^ index.php [QSA,L]
            </Directory>
    </VirtualHost>

## Ensure folders are writeable

Ensure that `/public/files`, `/files/tmp/twig` and `/public/images` are writeable for the web server user, ie `www-data` 

    sudo chown www-data:www-data public/files
    sudo chown www-data:www-data public/images
    sudo chown www-data:www-data files/tmp/twig

## Issues

* When uploadig multiple files, and one file exceeds upload_max_filesize in php.ini. No error message is printed.
