<?php
namespace Test\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface;
use Slim\Views\Twig;

class flashMiddleware
{

	private $container;

	public function __construct(\Psr\Container\ContainerInterface $container) {
		$this->container = $container;
	}

	public function __invoke(Request $request, RequestHandler $handler): ResponseInterface {

		$this->container->get('view')->getEnvironment()->addGlobal(
			'flash',
			$this->container->get('flash')
		);

		return $response = $handler->handle($request);
	}
}

