<?php

namespace Test\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ImageController extends \Test\BaseController
{
	private $ImageService;

	public function __construct(
		\Psr\Container\ContainerInterface $container,
		\Test\Domain\Services\Image\ImageService $imageService
	) {
		parent::__construct($container);

		$this->imageService = $imageService;
	}

	public function listImages(
		ServerRequestInterface $request, 
		ResponseInterface $response
	) : ResponseInterface {


		$settings = $this->container->get('settings');

		$params = [];

		$params['images'] = $this->imageService->scan(
			$settings['imageDirectory'],
			$settings['imageFileTypes']
		);

		$params['imageFileTypes'] = implode(
			', ',
			$settings['imageFileTypes']
		);

		$params['publicImageDirectory'] = str_replace(
			$settings['public'],
			'',
			$settings['imageDirectory']
		);

		return $this->container->get('view')->render(
			$response,
			'image/list.twig',
			$params
		);
	}

	public function upload(
		ServerRequestInterface $request, 
		ResponseInterface $response
	): ResponseInterface {

		$settings = $this->container->get('settings');

		$uploadedFiles = $request->getUploadedFiles();
		$fileCounter = 0;
		$errors = [];

		// TODO: Move more of this code into ImageService.php.
		foreach ($uploadedFiles['imageFiles'] as $uploadedFile) {

			if ($uploadedFile->getError() === UPLOAD_ERR_OK) {

				try {
					$this->imageService->moveUploadedFile(
						$settings['imageDirectory'],
						$uploadedFile,
						$settings['imageFileTypes']
					);
				} catch (\Exception $exception) {
					$errors[] = $exception->getMessage();
					continue;
				}

				$fileCounter++;
			} else if ($uploadedFile->getError()=== UPLOAD_ERR_INI_SIZE) {
				$errors[] = sprintf('%s exceeds upload_max_filesize (%s) in php.ini',
					$uploadedFile->getClientFilename(),
					ini_get('upload_max_filesize')
				);
			}
		}

		$message = sprintf('Uploaded %s image(s).',
			$fileCounter
		);

		if (count($errors) > 0) {
			$this->container->get('flash')->addMessage(
				'alert',
				implode(', ', $errors)
			);
		}

		if ($fileCounter > 0) {
			$this->container->get('flash')->addMessage('info', $message);
		}

		return $response->withHeader(
			'Location',
			'/'
		)->withStatus(302);
	}

	public function download(
		ServerRequestInterface $request, 
		ResponseInterface $response
	): ResponseInterface {

		$settings = $this->container->get('settings');
		$md5s = [];
		$zipFile = null;

		$uploadedFiles = $request->getUploadedFiles();

		if (!is_null($uploadedFiles['md5File'])) {
			$md5s = $this->imageService->readMd5File($uploadedFiles['md5File']);
		}

		$images = $this->imageService->scan(
			$settings['imageDirectory'],
			$settings['imageFileTypes']
		);

		$images = $this->imageService->filterOutMd5s(
			$images,
			$md5s
		);

		$md5File = $this->imageService->generateMd5File(
			$images,
			$md5s,
			$settings['tmpDirectory']
		);

		try {
			$zipFile = $this->imageService->generateZip(
				$images,
				$md5File,
				$settings['imageDirectory'],
				$settings['tmpDirectory']
			);
		} catch (\Exception $exception) {
			$this->container->get('flash')->addMessage(
				'alert',
				$exception->getMessage()
			);

			return $response->withHeader(
				'Location',
				'/'
			)->withStatus(302);
		}

		return $response->withHeader(
			'Location',
			sprintf('/files/%s', $zipFile)
		)->withStatus(302);
	}
}

?>
