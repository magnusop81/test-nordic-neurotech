<?php

namespace Test;

abstract class BaseController
{

	protected $container;

	function __construct(
		\Psr\Container\ContainerInterface $container
	) {
		$this->container = $container;
	}
}

?>
