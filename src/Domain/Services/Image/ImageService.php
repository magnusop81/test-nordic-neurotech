<?php

namespace Test\Domain\Services\Image;

use Slim\Psr7\UploadedFile;

final class ImageService {

	public function __construct() {
	}

	public function scan(
		string $directory,
		array $fileTypes
	) : array {

		if (!is_dir($directory)) {
			throw new \Exception('Invalid directory provided.');
		}

		$pattern = sprintf('%s%s*.{%s}',
			$directory,
			DIRECTORY_SEPARATOR,
			implode(',', $fileTypes)
		);

		$files = glob(
			$pattern,
			GLOB_BRACE
		);

		return $this->calculateMd5($files);
	}

	public function moveUploadedFile(
		string $directory,
		UploadedFile $uploadedFile,
		array $validExtensions
	) : string {

		$extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);

		if (!in_array($extension, $validExtensions)) {
			throw new \Exception('Invalid image type.');
		}

		// TODO: Improve file validation by also checking mime type

		$destination = sprintf('%s%s%s',
			$directory,
			DIRECTORY_SEPARATOR,
			$uploadedFile->getClientFilename()
		);

		$uploadedFile->moveTo($destination);

		return $destination;
	}

	public function readMd5File(
		UploadedFile $uploadedFile
	) : array {

		$md5sInFile = [];
		$output = [];

		if ($uploadedFile->getError() === UPLOAD_ERR_OK) {


			$content = (string)$uploadedFile->getStream();
			$md5sInFile = explode("\n", $content);
		}

		foreach ($md5sInFile as $md5) {
			if ($this->isValidMd5($md5) === false) {
				continue;
			}

			$output[] = $md5;
		}

		return $output;
	}

	/*
	 For simplicity I am reusing the same MD5 file reference for all request.
	 This is likely to cause collisions in a multi user environment.
	 A solid solution could be to create a directory with a uuidv4 as a
	 directory name and generate the md5 and zip file within that direcotry.
	*/
	public function generateMd5File(
		array $images,
		array $md5s,
		string $tmpDirectory
	) : string {


		/*
		 I am assuming that we want to include the previously downloaded md5s
		 in the new md5 file. This can easily be changed by skipping the
		 following array merge.
		*/
		$oldAndNewMd5s = array_merge(
			$md5s,
			array_values($images)
		);

		$file = sprintf('%s%sMD5.txt',
			$tmpDirectory,
			DIRECTORY_SEPARATOR
		);

		file_put_contents(
			$file,
			implode("\n", $oldAndNewMd5s)
		);

		return $file;
	}

	public function generateZip(
		array $images,
		string $md5File,
		string $imageDirectory,
		string $tmpDirectory
	) : string {

		// using timestamp as filename
		$file = sprintf('%s%s%s.zip',
			$tmpDirectory,
			DIRECTORY_SEPARATOR,
			time()
		);

		$zip = new \ZipArchive();

		$result = $zip->open(
			$file,
			\ZipArchive::CREATE
		);

		switch ($result) {
			case \ZipArchive::ER_EXISTS:
				$message = 'ER_EXISTS';
				break;
			case \ZipArchive::ER_INCONS:
				$message = 'ER_INCONS';
				break;
			case \ZipArchive::ER_INVAL:
				$message = 'ER_INVAL';
				break;
			case \ZipArchive::ER_MEMORY:
				$message = 'ER_MEMORY';
				break;
			case \ZipArchive::ER_NOENT:
				$message = 'ER_NOENT';
				break;
			case \ZipArchive::ER_NOZIP:
				$message = 'ER_NOZIP';
				break;
			case \ZipArchive::ER_OPEN:
				$message = 'ER_OPEN';
				break;
			case \ZipArchive::ER_READ:
				$message = 'ER_READ';
				break;
			case \ZipArchive::ER_SEEK:
				$message = 'ER_SEEK';
				break;
		}

		if ($result !== true) {
			throw new \Exception($message);
		}

		$zip->addFile(
			$md5File,
			'MD5.txt'
		);

		foreach ($images as $image => $md5) {

			$source = sprintf('%s%s%s',
				$imageDirectory,
				DIRECTORY_SEPARATOR,
				$image
			);
			$zip->addFile(
				$source,
				$image
			);
		}

		$zip->close();

		return basename($file);
	}

	public function filterOutMd5s(
		array $images,
		array $md5s
	) : array {

		$output = [];

		foreach ($images as $filename => $md5) {
			if (in_array($md5, $md5s)) {
				continue;
			}

			$output[$filename] = $md5;
		}

		return $output;
	}

	private function calculateMd5(
		array $files
	) : array {

		$output = [];

		foreach ($files as $file) {
			$output[basename($file)] = md5_file($file);
		}

		return $output;
	}

	private function isValidMd5(
		$md5
	) {

		if (empty($md5)) {
			return false;
		}

		return preg_match('/^[a-f0-9]{32}$/', $md5);
	}


}
