<?php

if (!function_exists('d')) {
	function d($variable) : void {
		print '<pre style="float:left;clear:left;">';
		if (is_object($variable)) {
			print_r($variable);
		} else if (is_array($variable)) {
			print_r($variable);
		} else {
			var_dump($variable);
		}
		print '</pre>';
	}
}
